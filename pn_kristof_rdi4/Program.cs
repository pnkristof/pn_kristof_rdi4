﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net;
using System.Xml.Linq;
using System.Threading;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Device.Location;


//Google Maps Geocoding API
namespace pn_kristof_rdi4
{
    class Program
    {
        static void Main(string[] args)
        {

            //beolvasás, feldolgozás
            string TextUrl = "http://www.szabozs.hu/_DEBRECEN/kerteszg/famousplaces.txt";
            List<string> places = GetPlaces(TextUrl).ToList();


            List<Task> ts = new List<Task>();
            foreach (var p in places)
            {
                Task t = new Task(() => { Feldolgozas(p); }, TaskCreationOptions.LongRunning);
                ts.Add(t);
                t.Start();
            }

            Task.WaitAll(ts.ToArray());
            //beolvasás, feldolgozás vége
            Console.ReadLine();

            Console.WriteLine();
            //1. szűrés: országok nevezetességek száma szerint rendezve
            var q1 = (from x in Places
                      group x by x.Location into g
                      orderby g.Count() descending
                      select new
                      {
                          Name = g.Key,
                          Count = g.Count()
                      }).Take(5);

            foreach (var item in q1)
            {
                Console.WriteLine(item.ToString());
            }

            /*
             * Eredmény:
             * United States 11
             * France 9
             * United Kingdom 5
             * Italy 5
             * Egypt 4
             * ...
             */


            Console.WriteLine();
            //2. szűrés: egymáshoz legközelebb lévő nevezetesség
            //linq lekérdezés nélkül:

            //double minDist = Places.First().Coord.GetDistanceTo(Places.Last().Coord);

            //FamousePlace min1 = new FamousePlace();
            //FamousePlace min2 = new FamousePlace();
            //foreach (var one in Places)
            //{
            //    foreach (var other in Places)
            //    {
            //        if (one.Coord.GetDistanceTo(other.Coord) < minDist && !one.Equals(other))
            //        {
            //            minDist = one.Coord.GetDistanceTo(other.Coord);
            //            min1 = one;
            //            min2 = other;

            //        }
            //    }
            //}
            //Console.WriteLine("Egymáshoz legközelebb: " + minDist + "m");
            //Console.WriteLine(min1.Name + "\n" + min2.Name);


            //lekérdezéssel:
            var q2 = (from pair in 
                           (from x in Places
                            select new
                            {
                                //minden helyhez hozzárendeljük azt a helyet, amelyhez legközelebb van, így hely párokat kapunk
                                p1 = x,
                                p2 = (from y in Places
                                      where !y.Equals(x)
                                      orderby x.Coord.GetDistanceTo(y.Coord)
                                      select y).First()
                            })
                     //a párokat rendezzük távolságok szerint
                     orderby pair.p1.Coord.GetDistanceTo(pair.p2.Coord)
                     select new
                     {
                         MinDisP1 = pair.p1,
                         MinDisP2 = pair.p2,
                         Dis = pair.p1.Coord.GetDistanceTo(pair.p2.Coord)

                     }).First();
            
            Console.WriteLine(q2.MinDisP1.ToString());
            Console.WriteLine(q2.MinDisP2.ToString());
            Console.WriteLine(q2.Dis);

            /*
             * Eredmény:
             * Al Aqsa (Jerusalem)
             * The Wailing Wall (Jerusalem)
             * Distance: 140,47 m
             */

            Console.WriteLine();
            //3. szűrés: egy adott országban legtávolabb lévő nevezetességek
            var q3 = (from pair in
                             (from x in Places
                              select new
                              {
                                  p1 = x,
                                  p2 = (from y in Places
                                        where x.Location == y.Location
                                        orderby x.Coord.GetDistanceTo(y.Coord) descending
                                        select y).First()
                              })
                              orderby pair.p1.Coord.GetDistanceTo(pair.p2.Coord) descending
                     select new
                     {
                         MaxDisP1 = pair.p1,
                         MaxDisP2 = pair.p2,
                         Dis = pair.p1.Coord.GetDistanceTo(pair.p2.Coord)
                     }).First();
                         


            Console.WriteLine(q3.MaxDisP1.ToString());
            Console.WriteLine(q3.MaxDisP2.ToString());
            Console.WriteLine(q3.Dis);

            /*
             * Eredmény:
             * Golden Gate Bridge (California, United States)
             * The Empire State Building (New Yor, United States)
             * Distance: 4 136 686,15 m
            */

            Console.ReadLine();
        }


        //betöltött txt feldarabolása
        static IEnumerable<string> GetPlaces(string url)
        {
            return (new WebClient()).DownloadString(url).Split('\n');
        }


        //XML betöltése
        static XDocument GetXML(string key)
        {
            string api = "AIzaSyBC9b_tjnPFCva-Q8JAiCC0anxP3v3nsKQ"; //igényeltem API kulcsot a gyorsabb feldolgozásért, csak megadott IP-ről használható
            return XDocument.Parse(new WebClient().DownloadString($"https://maps.googleapis.com/maps/api/geocode/xml?address={key}&key={api}"));
        }
        

        static SemaphoreSlim sem = new SemaphoreSlim(101);

        //letöltések kezelése
        static void Feldolgozas(string key)
        {


            string status;
            do
            {
                sem.Wait();
                Console.WriteLine("Letöltés: " + key);
                XDocument eredmeny = GetXML(key);
                //Console.WriteLine("Letöltve: " + key);
                sem.Release();

                status = (from x in eredmeny.Elements("GeocodeResponse")
                          select x.Element("status").Value).First();

                if (status == "OK")
                {
                    SavePlace(eredmeny, key);
                    Console.WriteLine($"Feldolgozva: {key}");

                }
                else if (status == "OVER_QUERY_LIMIT")
                {
                    Console.WriteLine($"Over query limit, újrapróbálkozás: {key}");
                    Thread.Sleep(1000);
                }
                else
                {
                    Console.WriteLine($"Pontatlan eredmény: {key}");
                }

            } while (status == "OVER_QUERY_LIMIT");
            //Console.WriteLine($"Befejezve: {key}");
        }


        static List<FamousePlace> Places = new List<FamousePlace>();
        //FamousePlace példányok létrehozása
        static void SavePlace(XDocument xd, string name)
        {
            var q = (from x in xd.Descendants("GeocodeResponse").Descendants("result")
                     select new FamousePlace
                     {
                         Name = name,
                         Address = x.Element("formatted_address").Value,
                         Location = (from y in x.Elements("address_component")
                                     where !y.Element("type").Value.Contains("postal_code") //irányítószámok kiszűrése
                                     select y.Element("long_name").Value).Last(), //az utolsó location element, ami nem irányítószám, általában az ország
                         Coord = new GeoCoordinate
                         (
                            double.Parse(x.Element("geometry").Element("location").Element("lat").Value.Replace('.', ',')),
                            double.Parse(x.Element("geometry").Element("location").Element("lng").Value.Replace('.', ','))
                         )
                     });
            Places.Add(q.First());
        }

    }


    class FamousePlace
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string Location { get; set; }
        public GeoCoordinate Coord;

        public override string ToString()
        {
            string str = "Név: " + Name + "\nElhelyezkedés: " + Location;
            return str;
        }


    }
}
